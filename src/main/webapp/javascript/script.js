var addTask = function() {
                      var task = {
                          action : $('#action').val(),
                          completed: false,
                          userId: null
                      }
                      $.ajax({
                          url: '/todolist/tasks/add',
                          type: 'POST',
                          dataType: 'json',
                          data: JSON.stringify(task),
                          contentType: 'application/json',
                          mimeType: 'application/json',
                          success: function(data) {
                              $('#action').val('');
                              $('#unfinished_tasks').last().prepend(
                                  '<tr id="' + data + '">' +
                                      '<td><div class="unfinished_task">' + task.action + '</div></td>' +
                                      '<td><div id="delete_task"><img src="/todolist/img/delete.png" /></div></td>' +
                                  '</tr>'
                              );
                          },
                          error: function() {
                              alert('Cannot create task');
                          }
                      });
                  }


$('#action').bind('enterKey', function(e) {
    addTask();
});

$('#action').keyup(function(e){
    if(e.keyCode == 13)
    {
        $(this).trigger("enterKey");
    }
});

$('.add_task').click(function() {
    addTask();
    }
);

$('#unfinished_tasks').on('click', '.unfinished_task', function() {
    var id = $(this).parent().parent().attr("id");
    var action = $(this).text();
    $.ajax({
        url: '/todolist/tasks/done',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(id),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function(data) {
            $('#' + id).remove();
            $('#finished_tasks').last().prepend(
                '<tr id="' + id + '">' +
                '<td><div class="finished_task">' + action + '</div></td>' +
                '</tr>'
            );
        },
        error: function() {
            alert('Cannon complete task');
        }
    });
});

$('#finished_tasks').on('click', '.finished_task', function() {
    var id = $(this).parent().parent().attr("id");
    var action = $(this).text();
    $.ajax({
        url: '/todolist/tasks/undone',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(id),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function(data) {
            $('#' + id).remove();
            $('#unfinished_tasks').last().prepend(
                '<tr id="' + id + '">' +
                    '<td><div class="unfinished_task">' + action + '</div></td>' +
                    '<td><div id="delete_task"><img src="/todolist/img/delete.png" /></div></td>' +
                '</tr>'
            );
        },
        error: function() {
            alert('Cannon uncomplete task');
        }
    });
});

$('#unfinished_tasks').on('click', '#delete_task', function() {
    var id = $(this).parent().parent().attr("id");
    $.ajax({
        url: '/todolist/tasks/delete',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(id),
        contentType: 'application/json',
        mimeType: 'application/json',
        success: function(data) {
            $('#' + id).remove();
        },
        error: function() {
            alert('Cannon delete task');
        }
    });
});