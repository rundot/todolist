<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <link rel="stylesheet" type="text/css" href="/todolist/css/style.css" />
    </head>
    <body>
        <div id="header">
            <form method="POST">
                <input type="text" id="login" name="login" placeholder="login" />
                <input type="password" id="password" name="password" placeholder="password" />
                <button id="login_btn" type="submit" name="action" value="login">Sign In</button>
                <button id="register_btn" type="submit" name="action" value="register">Sign Up</button>
            <form>
        </div>
        <div id="body">
            <h1>${sessionScope.status}</h1>
            <div id="info">
                You can use <b>admin:admin</b> credentials to see admin&#39s tasks<br />
                Or create your own user with custom toDo list<br />
                bitBucket repository: <a href="https://bitbucket.org/rundot/todolist/src">https://bitbucket.org/rundot/todolist/src</a>
            </div>
        </div>
    </body>
</html>