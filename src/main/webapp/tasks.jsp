<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="/todolist/css/style.css" />
        <title>ToDoList</title>
    </head>
    <body>
        <div id="header">
            <h1>Hello, ${sessionScope.fullname}. Here is your tasks for today.</h1>
        </div>
        <div id="body_tasks">
        <div id="unfinished_tasks_div">
            <div id="new_task">
                <input type="text" id="action" placeholder="New Task" />
                <button class="add_task">Add</button>
            </div>
            <center><table id="unfinished_tasks">
                <c:forEach var="task" items="${uncompletedTasks}">
                    <tr id="${task.id}">
                        <td>
                            <div class="unfinished_task">${task.action}</div>
                        </td>
                        <td>
                            <div id="delete_task"><img src="/todolist/img/delete.png" /></div>
                        </td>
                    </tr>
                </c:forEach>
            </table></center>
        </div>
        <div id="finished_tasks_div">
            <center><table id="finished_tasks">
                <c:forEach var="task" items="${completedTasks}">
                    <tr id="${task.id}">
                        <td><div class="finished_task">${task.action}</div></td>
                    </tr>
                </c:forEach>
            </table>
            </center>
        </div>
        </div>
        <script type="text/javascript" src="/todolist/javascript/jquery.js"></script>
        <script language="javascript" type="text/javascript" src="/todolist/javascript/script.js"></script>
    </body>
</html>