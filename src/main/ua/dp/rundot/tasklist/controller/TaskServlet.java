package ua.dp.rundot.tasklist.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ua.dp.rundot.tasklist.domain.Task;
import ua.dp.rundot.tasklist.service.TaskService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by emaksimovich on 19.12.16.
 */
public class TaskServlet extends HttpServlet {

    private Pattern taskListPattern = Pattern.compile("/todolist/tasks");
    private Pattern taskAddPattern = Pattern.compile("/todolist/tasks/add");
    private Pattern taskDeletePattern = Pattern.compile("/todolist/tasks/delete");
    private Pattern taskDonePattern = Pattern.compile("/todolist/tasks/done");
    private Pattern taskUndonePattern = Pattern.compile("/todolist/tasks/undone");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String requestedURI = req.getRequestURI();
        if (taskListPattern.matcher(requestedURI).matches()) {
            Long userId = (Long)req.getSession().getAttribute("user_id");
            List<Task> taskList = TaskService.listFiltered(userId);
            List<Task> completedTasks = TaskService.completed(taskList);
            List<Task> uncompletedTasks = TaskService.uncompleted(taskList);
            req.setAttribute("completedTasks", completedTasks);
            req.setAttribute("uncompletedTasks", uncompletedTasks);
            req.getRequestDispatcher("/tasks.jsp").forward(req, resp);
        }
        resp.sendError(404);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String requestedURI = req.getRequestURI();
        
        BufferedReader reader = new BufferedReader(new InputStreamReader(req.getInputStream()));
        String json = "";
        if (reader != null) {
            json = reader.readLine();
        }

        ObjectMapper mapper = new ObjectMapper();
        
        if (taskAddPattern.matcher(requestedURI).matches()) {
            Task task = mapper.readValue(json, Task.class);
            task.setUserId((long)req.getSession().getAttribute("user_id"));
            Long id = TaskService.save(task);
            if (id != null) {
                resp.setContentType("application/json");
                mapper.writeValue(resp.getOutputStream(), id);
            }
        }

        if (taskDeletePattern.matcher(requestedURI).matches()) {
            long id = mapper.readValue(json, long.class);
            int result = TaskService.delete(id);
            if (result > 0) {
                resp.setContentType("application/json");
                mapper.writeValue(resp.getOutputStream(), result);
            }
        }

        if (taskDonePattern.matcher(requestedURI).matches()) {
            long id = mapper.readValue(json, long.class);
            int result = TaskService.done(id);
            if (result > 0) {
                resp.setContentType("application/json");
                mapper.writeValue(resp.getOutputStream(), result);
            }
        }

        if (taskUndonePattern.matcher(requestedURI).matches()) {
            long id = mapper.readValue(json, long.class);
            int result = TaskService.unDone(id);
            if (result > 0) {
                resp.setContentType("application/json");
                mapper.writeValue(resp.getOutputStream(), result);
            }
        }
    }

}
