package ua.dp.rundot.tasklist.controller;

import ua.dp.rundot.tasklist.domain.User;
import ua.dp.rundot.tasklist.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by rundot on 20.12.2016.
 */
public class RegisterServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/register.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String fullname = req.getParameter("fullname");
        String status = "";

        if (login == "" || password == "") {
            status =  "You should fill all the fields";
        } else if (UserService.save(new User(0l, login, password, fullname)) > 0) {
            status = "User created. You can now sign in";
        } else {
            status = "User already exists";
        }

        req.setAttribute("status", status);
        req.getSession().setAttribute("status", status);
        resp.sendRedirect("/todolist/login");

    }
}
