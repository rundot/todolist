package ua.dp.rundot.tasklist.controller;

import ua.dp.rundot.tasklist.domain.User;
import ua.dp.rundot.tasklist.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by rundot on 20.12.2016.
 */
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");

        switch (action) {

            case "login": {
                String login = req.getParameter("login");
                String password = req.getParameter("password");
                if (UserService.isValid(login, password)) {
                    User user = UserService.get(login, password);
                    HttpSession session = req.getSession();
                    session.setAttribute("login", user.getLogin());
                    session.setAttribute("password", user.getPassword());
                    session.setAttribute("user_id", user.getId());
                    session.setAttribute("fullname", user.getFullName());
                    resp.sendRedirect("/todolist/tasks");
                } else {
                    req.getSession().setAttribute("status", "Incorrect login/password");
                    req.getRequestDispatcher("/login.jsp").forward(req, resp);
                }
                break;
            }

            case "register": {
                resp.sendRedirect("/todolist/register");
                break;}
        }

    }
}
