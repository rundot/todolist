package ua.dp.rundot.tasklist.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.sql.Timestamp;

/**
 * Created by emaksimovich on 19.12.16.
 */
@JsonAutoDetect
public class Task {

    private Long id;
    private String action;
    private Long userId;
    private boolean completed;
    private Timestamp timestamp;

    public Long getId() {
        return id;
    }

    public String getAction() {
        return action;
    }

    public Long getUserId() {
        return userId;
    }

    public boolean isCompleted() {
        return completed;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", action='" + action + '\'' +
                ", userId=" + userId +
                ", completed=" + completed +
                ", timestamp=" + timestamp +
                '}';
    }


    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Task() {
        this.timestamp = new Timestamp(System.currentTimeMillis());

    }

    public Task(Long id, String action, Long userId, boolean completed) {
        this.id = id;
        this.action = action;
        this.userId = userId;
        this.completed = completed;
        this.timestamp = new Timestamp(System.currentTimeMillis());

    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
