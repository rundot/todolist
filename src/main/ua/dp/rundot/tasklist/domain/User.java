package ua.dp.rundot.tasklist.domain;

/**
 * Created by rundot on 20.12.2016.
 */
public class User {

    private Long id;
    private String login;
    private String password;
    private String fullName;

    public User(Long id, String login, String password, String fullName) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.fullName = fullName;
    }

    public Long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getFullName() {
        return fullName;
    }
}
