package ua.dp.rundot.tasklist.dao;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import ua.dp.rundot.tasklist.domain.Task;

import java.util.List;

/**
 * Created by emaksimovich on 19.12.16.
 */
public interface TaskDao {

    Long save(Task task);

    int update(Task task);

    int delete(Long id);

    Task get(Long id);

    List<Task> list();

}
