package ua.dp.rundot.tasklist.dao;

import ua.dp.rundot.tasklist.domain.User;

import java.util.List;

/**
 * Created by rundot on 20.12.2016.
 */
public interface UserDao {

    Long save(User user);

    int update(User user);

    int delete(Long id);

    User get(Long id);

    List<User> list();

}
