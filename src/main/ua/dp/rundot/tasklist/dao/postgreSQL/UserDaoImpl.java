package ua.dp.rundot.tasklist.dao.postgreSQL;

import ua.dp.rundot.tasklist.dao.UserDao;
import ua.dp.rundot.tasklist.domain.User;
import ua.dp.rundot.tasklist.util.Logger;
import ua.dp.rundot.tasklist.util.SingleConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rundot on 20.12.2016.
 */
public class UserDaoImpl implements UserDao {

    private Connection connection = SingleConnection.getInstance();

    @Override
    public Long save(User user) {
        if (connection == null) return null;
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO USERS (login, password, fullname) " +
                        "VALUES (?, ?, ?) " +
                        "RETURNING id;"
        )) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getFullName());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Long id = resultSet.getLong("id");
            resultSet.close();
            return id;
        } catch (SQLException e) {
            Logger.log(e.getMessage());
        }
        return null;
    }

    @Override
    public int update(User user) {
        if (connection == null) return -1;
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE users SET " +
                        "login = ?, " +
                        "password = ?, " +
                        "fullname = ? " +
                        "WHERE id = ?;"
        )) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getFullName());
            statement.setLong(4, user.getId());
            return statement.executeUpdate();
        } catch (SQLException e) {
            Logger.log(e.getMessage());
        }
        return -1;
    }

    @Override
    public int delete(Long id) {
        if (connection == null) return -1;
        try (PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM users WHERE id = ?;"
        )) {
            statement.setLong(1, id);
            return statement.executeUpdate();
        } catch (SQLException e) {
            Logger.log(e.getMessage());
        }
        return -1;
    }

    @Override
    public User get(Long id) {
        if (connection == null) return null;
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM users WHERE id = ?;"
        )) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            User user = new User(
                    resultSet.getLong("id"),
                    resultSet.getString("login"),
                    resultSet.getString("password"),
                    resultSet.getString("fullname")
            );
            resultSet.close();
            return user;
        } catch (SQLException e) {
            Logger.log(e.getMessage());
        }
        return null;
    }

    @Override
    public List<User> list() {
        if (connection == null) return null;
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM users;");
            List<User> users = new ArrayList<>();
            while (resultSet.next()) {
                User user = new User(
                        resultSet.getLong("id"),
                        resultSet.getString("login"),
                        resultSet.getString("password"),
                        resultSet.getString("fullname")
                );
                users.add(user);
            }
            resultSet.close();
            return users;
        } catch (SQLException e) {
            Logger.log(e.getMessage());
        }
        return null;
    }
}
