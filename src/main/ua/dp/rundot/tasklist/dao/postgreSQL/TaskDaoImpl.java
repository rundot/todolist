package ua.dp.rundot.tasklist.dao.postgreSQL;

import ua.dp.rundot.tasklist.dao.TaskDao;
import ua.dp.rundot.tasklist.domain.Task;
import ua.dp.rundot.tasklist.util.SingleConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emaksimovich on 19.12.16.
 */
public class TaskDaoImpl implements TaskDao {

    private static Connection connection = SingleConnection.getInstance();

    @Override
    public Long save(Task task) {
        if (connection == null) return null;
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO tasks (action, completed, user_id, timestamp) " +
                        "VALUES (?, ?, ?, ?) RETURNING id;"
        )) {
            statement.setString(1, task.getAction());
            statement.setBoolean(2, task.isCompleted());
            if (task.getUserId() == null || task.getUserId() == 0)
                statement.setNull(3, java.sql.Types.INTEGER);
            else
                statement.setLong(3, task.getUserId());
            statement.setTimestamp(4, task.getTimestamp());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Long id = resultSet.getLong("id");
            resultSet.close();
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int update(Task task) {
        if (connection == null) return -1;
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE tasks SET " +
                        "action = ?, " +
                        "completed = ?, " +
                        "user_id = ?, " +
                        "timestamp = ? " +
                        "WHERE id = ?;"
        )) {
            statement.setString(1, task.getAction());
            statement.setBoolean(2, task.isCompleted());
            if (task.getUserId() == null || task.getUserId() == 0   )
                statement.setNull(3, java.sql.Types.INTEGER);
            else
                statement.setLong(3, task.getUserId());
            statement.setTimestamp(4, task.getTimestamp());
            statement.setLong(5, task.getId());
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public int delete(Long id) {
        if (connection == null) return -1;
        try (PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM tasks WHERE id = ?;"
        )) {
            statement.setLong(1, id);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public Task get(Long id) {
        if (connection == null) return null;
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM tasks WHERE id = ?;"
        )) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Task task = new Task(
                    resultSet.getLong("id"),
                    resultSet.getString("action"),
                    resultSet.getLong("user_id"),
                    resultSet.getBoolean("completed")
            );
            resultSet.close();
            return task;
        } catch (SQLException e) {
            e.printStackTrace();
        };
        return null;
    }

    @Override
    public List<Task> list() {
        if (connection == null) return null;
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM tasks ORDER BY timestamp DESC;");
            List<Task> taskList = new ArrayList<>();
            while (resultSet.next()) {
                Task task = new Task(
                        resultSet.getLong("id"),
                        resultSet.getString("action"),
                        resultSet.getLong("user_id"),
                        resultSet.getBoolean("completed")
                );
                taskList.add(task);
            }
            resultSet.close();
            return taskList;
        } catch (SQLException e) {
            e.printStackTrace();
        };
        return null;
    }

}
