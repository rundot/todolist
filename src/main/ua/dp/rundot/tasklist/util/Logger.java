package ua.dp.rundot.tasklist.util;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by emaksimovich on 16.12.16.
 */
public class Logger {

    private static Connection connection = SingleConnection.getInstance();

    public static void log(String text) {
        if (connection == null) return;
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO log(text) VALUES (?);");
        ){
            statement.setString(1, text);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}

