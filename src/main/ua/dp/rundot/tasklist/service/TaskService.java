package ua.dp.rundot.tasklist.service;

import ua.dp.rundot.tasklist.dao.TaskDao;
import ua.dp.rundot.tasklist.dao.postgreSQL.TaskDaoImpl;
import ua.dp.rundot.tasklist.domain.Task;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by emaksimovich on 19.12.16.
 */
public class TaskService {

    private static TaskDao taskDao = new TaskDaoImpl();

    public static Task get(Long id) {
        return taskDao.get(id);
    }

    public static List<Task> list() {
        return taskDao.list();
    }

    public static List<Task> completed(List<Task> taskList) {
        return taskList.stream()
                .filter(task -> task.isCompleted())
                .collect(Collectors.toList());
    }

    public static List<Task> uncompleted(List<Task> taskList) {
        return taskList.stream()
                .filter(task -> !task.isCompleted())
                .collect(Collectors.toList());
    }

    public static List<Task> listFiltered(Long userId) {
        return taskDao.list()
                .stream()
                .filter(task -> task.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    public static Long save(Task task) {
        return taskDao.save(task);
    }

    public static int delete(Long id) {
        return taskDao.delete(id);
    }

    public static int done(Long id) {
        Task task = taskDao.get(id);
        task.setCompleted(true);
        return taskDao.update(task);
    }

    public static int unDone(Long id) {
        Task task = taskDao.get(id);
        task.setCompleted(false);
        return taskDao.update(task);
    }


}
