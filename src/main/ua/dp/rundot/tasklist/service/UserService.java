package ua.dp.rundot.tasklist.service;

import ua.dp.rundot.tasklist.dao.UserDao;
import ua.dp.rundot.tasklist.dao.postgreSQL.UserDaoImpl;
import ua.dp.rundot.tasklist.domain.User;

/**
 * Created by rundot on 20.12.2016.
 */
public class UserService {

    private static UserDao userDao = new UserDaoImpl();

    public static Long save(User user) {
        return userDao.save(user);
    }

    public static User get(Long id) {
        return userDao.get(id);
    }

    public static boolean isValid(String login, String password) {
        return userDao.list()
                .stream()
                .filter(user -> user.getLogin().equals(login) && user.getPassword().equals(password))
                .count() > 0;
    }

    public static User get(String login, String password) {
        return userDao.list()
                .stream()
                .filter(user -> user.getLogin().equals(login) && user.getPassword().equals(password))
                .findFirst()
                .get();
    }

}
